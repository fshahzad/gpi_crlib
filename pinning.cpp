/*
********************************************************************************
*                                                                              *
*     C functions for pinning processes on Linux with PLPA                     *
*                                                                              *
*     AUTHORS: Thomas ZEISER, RRZE, Universitaet Erlangen (Germany)            *
*              ( - 2010)            thomas.zeiser@rrze.uni-erlangen.de         *
*              Markus WITTMANN, RRZE, Universitaet Erlangen (Germany)          *
*              (2010)             markus.wittmann@rrze.uni-erlangen.de         *
*                                                                              *
*     Acknowledgements:                                                        *
*                      based on an example from Georg Hager, RRZE              *
*                                                                              *
********************************************************************************
*
*  THIS SOURCE FILE CONTAINS EXTENSIONS FROM RRZE WHICH DO NOT BELONG
*  TO THE DC-SOFTWARE-CORE !!
*  SPECIAL CONDIIONS MAY THEREFORE APPLY TO THE PRESENT SOURCES.
*  CONTACT hpc@rrze.uni-erlangen.de FOR DETAILS IF NECESSARY.
*
********************************************************************************
*
*     CVS Keywords:
*     ----------------
*     $Revision$
*         $Date$
*       $Author$
*
********************************************************************************
*/
#include <stdio.h>
#ifdef WIN32

  #define WIN32_LEAN_AND_MEAN
  #include <windows.h>

#elif defined(__sun)

  #include <stdlib.h>
  #include <sys/types.h>
  #include <sys/procset.h>
  #include <sys/processor.h>

#else

  #ifdef HAVE_PLPA
      #include <plpa.h>
  #else

    #ifndef _GNU_SOURCE
      #define _GNU_SOURCE
    #endif

    #include <stdlib.h>
    #include <sched.h>
    #include <errno.h>

//     #ifdef __PGI
//       // PGI brings its own sched.h stub and defines CPU_SET/CPU_ZERO incorrect
//       // here we provide a correction
//
//       #define __CPU_ZERO(set)   __CPU_ZERO_S(sizeof(cpu_set_t), set)
//       #define __CPU_SET(cpu, set)  __CPU_SET_S(cpu, sizeof(cpu_set_t), set)
//     #endif

  #endif // HAVE_PLPA

#endif // WIN32

#define ERROR_PIN_BASE              (-1000)
#define ERROR_PIN_NOT_IMPLEMTED     (ERROR_PIN_BASE - 1)


int env_var_to_i(const char * env, int * env_set, int default_value)
{
  char * env_var = getenv(env);

  if (env_var == NULL) {
    return default_value;
  }

  *env_set = 1;
  return atoi(env_var);
}


// --------------------------------------------------------------------------
//  pins the calling/current thread to the specified cpu number
// --------------------------------------------------------------------------

int pin_current_thread_to_cpu(int * cpu)
{
	int err = ERROR_PIN_NOT_IMPLEMTED;
	
  #ifdef WIN32

    SetThreadAffinityMask(GetCurrentThread(), 1 << *cpu);
//     printf("pinning via SetThreadAffinityMask.\n");
    // TODO: check return value / GetLastError()

  #elif defined(__sun)

    err = processor_bind(P_LWPID, P_MYID, *cpu, NULL);
// 	printf("pinning via processor_bind.\n");
    if (err != 0) {
      printf("pinning thread to cpu %d failed (processor bind error code %d: %s).\n", cpu, err, strerror(err));
      THROW("Pinning failed.");
    }

  #else

    #ifdef HAVE_PLPA
      plpa_cpu_set_t cpu_set;
      PLPA_CPU_ZERO(&cpu_set);
      PLPA_CPU_SET(*cpu, &cpu_set);

      err = plpa_sched_setaffinity((pid_t)0, sizeof(plpa_cpu_set_t), &cpu_set);
//       printf("pinning via plpa_sched_setaffinity.\n");
    #else
      cpu_set_t cpu_set;
      CPU_ZERO(&cpu_set);
      CPU_SET(*cpu, &cpu_set);

      err = sched_setaffinity((pid_t)0, sizeof(cpu_set_t), &cpu_set);
// 	printf("pinning via sched_setaffinity.\n");
    #endif

    if (err != 0) {
//       ErrorPrint("pinning thread to cpu %d failed (plpa error code %d: %s).\n", cpu, err, strerror(err));
//       THROW("Pinning failed.");
    }
  #endif

  return err;
}


// --------------------------------------------------------------------------
//  pins the calling/current thread to the specified cpu number
//  or computes the cpu number out of environment variables
// --------------------------------------------------------------------------

// 0,1,2_6,7,8

int pin_current_thread(int rank, int local_rank, int thread_id, int * cpu)
{
  char * cpu_list = new char[128] ;
  sprintf(cpu_list, "%s", "0,1,2,3,4,5,6,7,8,9,10,11");
  char * c = cpu_list;
  int i = 0;
  int t = 0;

  *cpu = -1;
	
//   sprintf(*cpu_list, "%s", '0');
  
  *cpu_list = '0';
  *c = '0';
  
  if (cpu_list == NULL) {
    return 0;
  }

  // cpu list is in the format of 0,1,2_3,4,5
  while (((*c >= '0' && *c <= '9') || *c == ',' || *c == '_')) {
    ++c;
  }

  if (*c != 0x00) {
    // invalid character detected
    return -1000;
  }

  c = cpu_list;

  while (i < local_rank && *c != 0x00) {
    if (*c == '_') ++i;
    ++c;
  }

  if (i != local_rank || *c < '0' || *c > '9') {
    // local list not found
    return -1001;
  }


  while (t < thread_id && *c != 0x00) {
    if (*c == ',') {
      ++t;
    }
    else if (*c == '_') {
      break;
    }

    ++c;
  }

  if (t != thread_id || *c < '0' || *c > '9') {
    // local cpu not found
    return -1002;
  }

  *cpu = atoi(c);

  return pin_current_thread_to_cpu(cpu);
}

int pin_current_master_thread(int rank, int local_rank, int thread_id, int * cpu)
{
  char * cpu_list = getenv("PIN_CP_CPULIST");
  char * c = cpu_list;
  int i = 0;
  int t = 0;

  *cpu = -1;

  if (cpu_list == NULL) {
    return 0;
  }

  // cpu list is in the format of 0,1,2_3,4,5
  while (((*c >= '0' && *c <= '9') || *c == ',' || *c == '_')) {
    ++c;
  }

  if (*c != 0x00) {
    // invalid character detected
    return -1000;
  }

  c = cpu_list;

  while (i < local_rank && *c != 0x00) {
    if (*c == '_') ++i;
    ++c;
  }

  if (i != local_rank || *c < '0' || *c > '9') {
    // local list not found
    return -1001;
  }


  while (t < thread_id && *c != 0x00) {
    if (*c == ',') {
      ++t;
    }
    else if (*c == '_') {
      break;
    }

    ++c;
  }

  if (t != thread_id || *c < '0' || *c > '9') {
    // local cpu not found
    return -1002;
  }

  *cpu = atoi(c);

  return pin_current_thread_to_cpu(cpu);
}


int pin_get_current_cpu(int * cpu)
{
  int i;

  int err = ERROR_PIN_NOT_IMPLEMTED;
  *cpu = -1;

  #ifdef WIN32
    // GetThreadAffinityMask(GetCurrentThread(), 1 << coreNumber);
  #elif defined(__sun)
    // not implemented
  #else

    #ifdef HAVE_PLPA

      plpa_cpu_set_t cpu_set;
      PLPA_CPU_ZERO(&cpu_set);
      PLPA_CPU_SET(coreNumber, &cpu_set);

      err = plpa_sched_getaffinity((pid_t)0, sizeof(plpa_cpu_set_t), &cpu_set);

      for (i = 0; i < PLPA_BITMASK_CPU_MAX; ++i) {
        if (PLPA_CPU_ISSET(i, &cpu_set)) {
          *cpu = i;
          break;
        }
      }

    #else
      cpu_set_t cpu_set;
      CPU_ZERO(&cpu_set);

      err = sched_getaffinity((pid_t)0, sizeof(cpu_set_t), &cpu_set);

      // constant CPU_SETSIZE is one larger than the maximum CPU
      // number that can be stored in a CPU set
      for (i = 0; i < CPU_SETSIZE; ++i) {
        if (CPU_ISSET(i, &cpu_set)) {
          *cpu = i;
          break;
        }
      }
    #endif
  #endif

  return err;
}
