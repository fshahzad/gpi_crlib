include ./make.defines

CXX = mpiCC
CF = mpif90
CFFLAGS += -fPIC -openmp -O3 -warn all 
CPPFLAGS +=  -fPIC -Wall -openmp -openmp-link=static -pthread -O3 -pedantic $(INCLUDES) $(DEFINES)
LIBS +=
INCLUDES +=
TARGET = libcrlib.a
LFLAGS =

OBJS = crlib.o pinning.o timing.o

$(TARGET): $(OBJS)
	ar rvs $(TARGET) $(OBJS)

timing.o:  timing.h timing.c
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(LIBS) -c timing.c

crlib.o: crlib.cpp crlib.h
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(LIBS) -c crlib.cpp

#test_utils.o: test_utils.h test_utils.c
#	$(CXX) $(CPPFLAGS) $(LIB_PATH) $(LIBS) -c test_utils.c

pinning.o: pinning.h pinning.cpp
	$(CXX) $(CPPFLAGS) $(INCLUDES) $(LIBS) -c pinning.cpp



clean:
	rm -f *.o *.a
