#include "crlib.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <stdarg.h>
#include <assert.h>
#include <iomanip>
#include <sched.h>
#include "pinning.h"


void gaspi_printf_array_(char * print_str, int * array_ptr, int num_elem){
	gaspi_printf("crlib: %s", print_str);
	for(int i=0;i<num_elem ;++i){
		gaspi_printf("crlib: %d\n", array_ptr[i]);
	}
}
void gaspi_printf_array_(char * print_str, gaspi_rank_t * array_ptr, int num_elem){
	gaspi_printf("crlib: %s", print_str);
	for(int i=0;i<num_elem ;++i){
		gaspi_printf("crlib: %hi\n", array_ptr[i]);
	}
}

gaspi_rank_t get_numprocs(){
	gaspi_rank_t numprocs_temp;
	gaspi_proc_num(&numprocs_temp);
	return numprocs_temp;
}
gaspi_rank_t get_myrank(){
	gaspi_rank_t myrank_temp;
	gaspi_proc_rank(&myrank_temp);
	return myrank_temp;
}


CR_THREAD::CR_THREAD(gaspi_rank_t numprocs_working_): numprocs(get_numprocs()), myrank(get_myrank()), numprocs_working(numprocs_working_) {
	restart= false;
	program_running_flag= true;										// TODO: check if it is necessary at all; otherwise delete it 
	to_neighbour=true, to_pfs=false;								// TODO: take this input from user and check for to_pfs function

	
	gaspi_printf("myrank: %d, numprocs: %d, numprocs_working %d \n ", myrank, numprocs, numprocs_working);
	myrank_active = myrank;
	machine_file_daemon= new char[256];
	cr_pthread = new pthread_t[1];
	perm_ckpt_dir_path = new char[256];
	temp_ckpt_dir_path = new char[256];
	main_ckpt_dir = new char[256];
	username = new char[256];
	
	sprintf(username, "%s", getenv("USER"));
	sprintf(tomove_ckpt_dir_name, "%s", "");
	sprintf(perm_ckpt_dir_path, "%s", "");
	sprintf(temp_ckpt_dir_path, "%s", "/tmp/");
	sprintf(prtr_ckpt_dir_path, "%s", "");

	
// 	nodenames_alive				=new char*[numprocs];					
	nodenames_all				=new char*[numprocs];
// 	partner_nodenames_alive			= new char*[numprocs];
// 	source_partner_nodenames_alive	= new char*[numprocs];				// TODO:check if it can be avoided
	
	
	transfer_CP_flag=false;

}


CR_THREAD::~CR_THREAD(){
	
}

int CR_THREAD::init(char * machine_file_daemon_, int CP_count_cr_thread_, int * status_processes_, char * perm_ckpt_dir_path_, char * temp_ckpt_dir_path_){
	
	int arg = 0;
	sprintf(perm_ckpt_dir_path, "%s", perm_ckpt_dir_path_);
	sprintf(temp_ckpt_dir_path, "%s", temp_ckpt_dir_path_);
	
	sprintf(machine_file_daemon, "%s", machine_file_daemon_);
	
	CP_count_cr_thread = CP_count_cr_thread_;
	status_processes = status_processes_;
	
// 	numprocs_working 			= numprocs_working_;
	nodenames_working			= new char*[numprocs_working];
	partner_nodenames			= new char*[numprocs_working];
	source_partner_nodenames 	= new char*[numprocs_working];
	pthread_create (cr_pthread, NULL, (void *(*)(void *)) &run_daemon_thread, this);
	if(crlib_verbose>=3) gaspi_printf_array_("crlib: status_processes\n", status_processes_, numprocs);

	if(crlib_verbose>=2) gaspi_printf("crlib: machine_file_daemon in init() function is %s\n", machine_file_daemon);
	
	return 0;
}

// int status_processes[]

char* CR_THREAD::mynodename(){
	char * my_node_name_=new char[256];
	char ** nodenames_alive_;
	nodenames_alive_	=new char*[numprocs];
	for(int i=0;i<numprocs;++i){
		nodenames_alive_[i]	=new char[256];
	}
	FILE * fp;
	if( NULL == (fp = fopen(machine_file_daemon, "r")) ) {
		gaspi_printf("crlib: Error: Unable to open file (%s)\n", machine_file_daemon);
		exit(-1);
	}

	for(int i=0;i<numprocs;++i)		/// make it till end-of-file for FILE *
	{
		fscanf(fp, "%s\n", nodenames_alive_[i]);
	}
	fclose(fp);

	my_node_name_ = nodenames_alive_[myrank];		// TODO: limitation: It will only work if 1 procs/node.
	return my_node_name_;
}

int CR_THREAD::make_nodelist(){		// every procses has the nodeslist of all the nodes, In GPI it is read from the machine files
	
	if(crlib_verbose>=3) gaspi_printf("crlib: machine_file_daemon %s\n", machine_file_daemon);
	for(int i=0;i<numprocs;++i){
// 		nodenames_alive[i]	=new char[10];
		nodenames_all[i]	=new char[10];
	}
	for(int i=0;i<numprocs_working;++i){
		nodenames_working[i]	=new char[10];
	}
	
	FILE * fp;
	if( NULL == (fp = fopen(machine_file_daemon, "r")) ) {
		gaspi_printf("crlib: Error: Unable to open file (%s)\n", machine_file_daemon);
		exit(-1);
	}

	for(int i=0;i<numprocs;++i)
	{
		fscanf(fp, "%s\n", nodenames_all[i]);
		nodenames_all[i] = nodenames_all[i];
	}
	for(int i=0;i<numprocs_working;++i)
	{
		nodenames_working[i] = nodenames_all[i];
		if(crlib_verbose>=3) gaspi_printf("crlib: nodenames_working[%d] %s\n", i, nodenames_working[i]);
	}
	fclose(fp);
	
	return 0;
}

int CR_THREAD::make_partner_nodelist(){
	for(int i=0;i<numprocs_working;++i){
		partner_nodenames[i]		= new char[10];
		source_partner_nodenames[i] = new char[10];
// 		partner_nodenames_alive[i]			= new char[10];
// 		source_partner_nodenames_alive[i]	= new char[10];
	}
	
	// create partner_nodenames_alive 
	for(int i=0;i<numprocs_working-1;++i){
		partner_nodenames[i]= nodenames_working[i+1];
// 		partner_nodenames_alive[i]=nodenames_alive[i+1];
	}
	partner_nodenames[numprocs_working-1] = nodenames_working[0];
// 	partner_nodenames_alive[numprocs-1]=nodenames_alive[0];
	
	// create source_partner_nodenames_alive
	for(int i=1;i<numprocs_working;++i){
		source_partner_nodenames[i] = nodenames_working[i-1];
// 		source_partner_nodenames_alive[i]=nodenames_alive[i-1];
	}
	source_partner_nodenames[0]=nodenames_working[numprocs_working-1];
	
	
// 	source_partner_nodenames_alive[0]=nodenames_alive[numprocs-1];
	if(status_processes[myrank]==WORKING_){
		if(crlib_verbose>=1) gaspi_printf("crlib: Myrank: %d, nodename: %s, partner_nodename: %s source_partner_nodenames: %s\n", myrank, mynodename(), partner_nodenames[myrank_active], source_partner_nodenames[myrank_active]);
	}
	return 0;
}



void run_daemon_thread(void * arg){
// 	CR_THREAD * cr_thread = new CR_THREAD(((CR_THREAD *)arg).numprocs_working);
	
	CR_THREAD * cr_thread = (CR_THREAD *) arg;
	cr_thread->make_nodelist();
	cr_thread->make_partner_nodelist();

	int core_num=23;
	pin_current_thread_to_cpu(&core_num);				// TODO: get this topology information 
	
	gaspi_rank_t myrank 			= cr_thread->myrank;
	gaspi_rank_t numprocs 			= cr_thread->numprocs;
	gaspi_rank_t numprocs_working 	= cr_thread->numprocs_working;
	
	gaspi_printf("2 myrank: %d, numprocs: %d, numprocs_working %d \n ", myrank, numprocs, numprocs_working);	
	if(myrank==0){
		cr_thread->write_partner_nodenames_info();
	}
	while(cr_thread->status_processes[myrank]==IDLE_){
		/// IDLE process will do nothing unless work is assigned 
		usleep(100000);
	}
	while(cr_thread->status_processes[myrank]==WORKING_){		// when status_processes[myrank]==WORK_FINISHED, loop will break.
// 		gaspi_printf("crlib:%d===== th: status_processes working=====\n", myrank);	
		if(cr_thread->transfer_CP_flag==true){	// new CP is there
			if(crlib_verbose>=1) gaspi_printf("crlib: cr_thread->transfer_CP_flag is : %d thus, transfering checkpoint...%d\n", cr_thread->transfer_CP_flag, cr_thread->CP_count_cr_thread);
			double ST_transfer_CP=0.0, ET_transfer_CP=0.0;
			get_walltime_(&ST_transfer_CP);
			cr_thread->transfer_CP();
			get_walltime_(&ET_transfer_CP);
			if(crlib_verbose>=1) gaspi_printf("crlib: transfer_CP time: %f\n", ET_transfer_CP - ST_transfer_CP);
			cr_thread->transfer_CP_flag=false;			/// this flag can be used to check the completion of the CP transfer operation.
		}
		usleep(100000);
	}
	gaspi_printf("crlib: myrank:%d .thread_finished work\n", myrank);
	return;
}


int CR_THREAD::transfer_CP(){
	if(to_neighbour==true){
		sprintf(main_ckpt_dir, "%s/CP-%s/", temp_ckpt_dir_path, nodenames_working[myrank_active]);
		gaspi_printf("crlib: main_ckpt_dir: %s\n", main_ckpt_dir);
		trasfer_CP_to_neighbour();
		gaspi_printf("crlib: trasfer_CP_to_neighbour() is complete\n");
	}
// 	if(to_pfs==true){						/// TODO: control the frequency of the PFS dumps
// 		transfer_CP_to_PFS();
// 		sync();
// // 		remove_prev_CP_PFS();
// 	}
	return 0;
}

int CR_THREAD::trasfer_CP_to_neighbour(){
	
// 	my_source_partner_nodename
	int CP_num_to_copy=CP_count_cr_thread;
	if(crlib_verbose>=2) gaspi_printf("crlib: crlib: CP_num_to_copy is: %d\n", CP_num_to_copy);
	sprintf(tomove_ckpt_dir_name, "%s", main_ckpt_dir);
// 	sprintf(tomove_ckpt_dir_name, "%s", "/tmp/Proc-*");
	sprintf(prtr_ckpt_dir_path, "/tmp/CP-%s", source_partner_nodenames[myrank_active]);
	mkdir(prtr_ckpt_dir_path, 0700);
	char cp_command[256]="";
	if(crlib_verbose>=2) gaspi_printf("crlib: ====myrank active: %d, my nodename: %s, my partner_nodename: %s source_partner_nodenames: %s\n", myrank_active, nodenames_working[myrank_active], partner_nodenames[myrank_active], source_partner_nodenames[myrank_active]);
	double rsync_start, rsync_end;
	
	sprintf(cp_command, "rsync -r --delete-after %s %s@%s:/tmp/CP-%s", tomove_ckpt_dir_name, username, partner_nodenames[myrank_active], nodenames_working[myrank_active]);	// copying local ckpt to remote dir
// 	sprintf(cp_command, "scp -r %s %s@%s:/tmp/CP-%s", tomove_ckpt_dir_name, username, partner_nodenames_alive[myrank_active], nodenames_alive[myrank_active]);	// copying local ckpt to remote dir // --ignore-existing option can hav problem if the meta-data file on nodes have same name over several iterations.
	
	if(crlib_verbose>=2)	gaspi_printf("crlib: cp_command: %s\n", cp_command );
	
	get_walltime_(&rsync_start); 
	system (cp_command);
// 	if(myrank==0){
// 		gaspi_printf("cp_command: %s\n", cp_command );
// 		system("rsync -r --delete-after --size-only --ignore-existing --bwlimit=100000 /tmp//CP-e0842/ unrza354@e0841:/tmp/CP-e0842");
// 	}
	get_walltime_(&rsync_end);
	
	if(crlib_verbose>=2) gaspi_printf("crlib: ============================ RSYNC time taken was: %f ============================\n", rsync_end - rsync_start);
	
	// TODO:	check the consistency of the data that has been coppied. and update the 'updated_CP_number.ckpt' file on it.
	return 0;
}

int CR_THREAD::is_CP_transfer_complete(){
	if(transfer_CP_flag==false){
		if(crlib_verbose>=2) gaspi_printf("crlib: transfer_CP_flag is false, thus CP transfer has been compelted\n");
		return true;
	}
	else{
		if(crlib_verbose>=2) gaspi_printf("crlib: transfer_CP_flag is true, thus CP transfer is not compelted\n");
		return false;
	}
}

int CR_THREAD::wait_for_CP_transfer(){
	while(transfer_CP_flag==true){
		if(crlib_verbose>=2) gaspi_printf("crlib: waiting for CP transfer to be completed\n");
		usleep(1000000);
	}
	return 0;
}

int CR_THREAD::remove_prev_CP_neighbour(){
	
	return 0;
}


int CR_THREAD::transfer_CP_to_PFS(){
// 	sprintf(main_ckpt_dir, "%s/CP-%s/", temp_ckpt_dir_path, nodenames_alive[myrank]);
	int CP_num_to_copy=CP_count_cr_thread;
	sprintf(tomove_ckpt_dir_name, "%s/CP-%d", temp_ckpt_dir_path, CP_num_to_copy);
	char cp_command[256]="";
	char main_perm_dir_path[256]="";
	sprintf(main_perm_dir_path, "%s", perm_ckpt_dir_path);
	mkdir(main_perm_dir_path, 0700);
	sprintf(main_perm_dir_path, "%s/CP-%s", main_perm_dir_path, nodenames_all[myrank]);
	mkdir(main_perm_dir_path, 0700);
	sprintf(cp_command, "cp -r %s %s", tomove_ckpt_dir_name, main_perm_dir_path);	// copies the whole directries CP (for all procs )
	if(crlib_verbose>=2)	gaspi_printf("crlib: cp_command: %s\n", cp_command);
	system (cp_command);
	sync();
	return 0;
}

/*
int CR_THREAD::fetch_remote_cp( int new_myrank_active, gaspi_rank_t * rescue_proc_list, int cp_to_restart){
	refresh_myrank_active(new_myrank_active);
	if(crlib_verbose>=3)	gaspi_printf_array_("crlib: rescue_proc_list:\n", rescue_proc_list, rescue_proc_list[0]+1);
	if(crlib_verbose>=2) 	gaspi_printf("crlib: %d: restart form: %d \n", myrank, cp_to_restart);
	
	bool am_i_rescue_proc = am_i_rescue_process_(rescue_proc_list);
	
	
	struct stat st;
	char * local_dirname= new char[256];
	char * to_fetch_dirname = new char[256];
	if(am_i_rescue_proc==false){
		if(crlib_verbose>=2) gaspi_printf("crlib: ==== checking for local directory for non-rescue procs ==== \n");
 		sprintf(local_dirname, "/tmp/CP-%s/Proc-%d_CP/CP-%d", nodenames_working[myrank_active], myrank_active,  cp_to_restart);
		if(stat(local_dirname, &st) == 0){
			if(crlib_verbose>=1) gaspi_printf("crlib:  %s is present locally\n", local_dirname);								// TODO:	check the sub dir too
		}
		if(stat(local_dirname, &st) != 0){
			if(crlib_verbose>=1) gaspi_printf("crlib:  %s is not present locally\n", local_dirname);							// TODO:	check the sub dir too
		}
	}
	if(am_i_rescue_proc==true){
		read_partner_nodenames_info();
		if(crlib_verbose>=2) gaspi_printf("crlib: ==== I have to bring CP from the failed procs partner ==== \n");
		char * main_ckpt_dir = new char[256];
		char * to_fetch_cmd = new char[256];
		
	 	sprintf(main_ckpt_dir, "/tmp/CP-%s", mynodename() );
		mkdir(main_ckpt_dir, 0700);
		sprintf(main_ckpt_dir, "%s/Proc-%d_CP/", main_ckpt_dir, myrank_active);
		mkdir(main_ckpt_dir, 0700);
		if(crlib_verbose>=2) gaspi_printf("crlib: main_ckpt_dir: %s\n", main_ckpt_dir);
		for(int i=0;i<numprocs_working;++i){
			gaspi_printf("crlib: partner_nodenames[%d]: %s\n", i, partner_nodenames[i]);
		}
		sprintf(to_fetch_dirname, "%s@%s:/tmp/CP-%s/Proc-%d_CP/CP-%d ", username, partner_nodenames[myrank_active], nodenames_working[myrank_active], (myrank_active), cp_to_restart);
		
		if(crlib_verbose>=2) gaspi_printf("crlib: to_fetch_dirname: %s\n", to_fetch_dirname);
		
		sprintf(to_fetch_cmd, "rsync -W -r %s %s", to_fetch_dirname, main_ckpt_dir);
		if(crlib_verbose>=2) gaspi_printf("crlib: to_fetch_cmd %s \n", to_fetch_cmd);
		system (to_fetch_cmd);
	}
	if(crlib_verbose>=2) gaspi_printf("crlib: fetch_remote_cp ends\n");
	return 0;
}*/

int CR_THREAD::fetch_remote_cp(gaspi_rank_t new_myrank_active, gaspi_rank_t * rescue_proc_list){
	refresh_myrank_active(new_myrank_active);
	if(crlib_verbose>=3)	gaspi_printf_array_("crlib: rescue_proc_list:\n", rescue_proc_list, rescue_proc_list[0]+1);
	
	gaspi_printf("1_myrank: %d, numprocs: %d, numprocs_working %d \n ", myrank, numprocs, numprocs_working);
	bool am_i_rescue_proc = am_i_rescue_process_(rescue_proc_list);
	
	
	struct stat st;
	char * local_dirname= new char[256];
	char * to_fetch_dirname = new char[256];
	if(am_i_rescue_proc==false){
		if(crlib_verbose>=2) gaspi_printf("crlib: ==== checking for local directory for non-rescue procs ==== \n");
 		sprintf(local_dirname, "/tmp/CP-%s", nodenames_working[myrank_active]);
		if(stat(local_dirname, &st) == 0){
			if(crlib_verbose>=1) gaspi_printf("crlib:  %s is present locally\n", local_dirname);								// TODO:	check the sub dir too
		}
		if(stat(local_dirname, &st) != 0){
			if(crlib_verbose>=1) gaspi_printf("crlib:  %s is not present locally\n", local_dirname);							// TODO:	check the sub dir too
		}
	}
	if(am_i_rescue_proc==true){
		read_partner_nodenames_info();
		if(crlib_verbose>=2) gaspi_printf("crlib: ==== I have to bring CP from the failed procs partner ==== \n");
		char * main_ckpt_dir = new char[256];
		char * to_fetch_cmd = new char[256];
		
	 	sprintf(main_ckpt_dir, "/tmp/CP-%s/", mynodename() );
		mkdir(main_ckpt_dir, 0700);
		if(crlib_verbose>=2) gaspi_printf("crlib: main_ckpt_dir: %s\n", main_ckpt_dir);
		for(int i=0;i<numprocs_working;++i){
			gaspi_printf("crlib: partner_nodenames[%d]: %s\n", i, partner_nodenames[i]);
		}
		sprintf(to_fetch_dirname, "%s@%s:/tmp/CP-%s/ ", username, partner_nodenames[myrank_active], nodenames_working[myrank_active]);
		
		if(crlib_verbose>=2) gaspi_printf("crlib: to_fetch_dirname: %s\n", to_fetch_dirname);
		
		sprintf(to_fetch_cmd, "rsync -r %s %s", to_fetch_dirname, main_ckpt_dir);
		if(crlib_verbose>=2) gaspi_printf("crlib: to_fetch_cmd %s \n", to_fetch_cmd);
		system (to_fetch_cmd);
	}
	if(crlib_verbose>=2) gaspi_printf("crlib: fetch_remote_cp ends\n");
	return 0;
}

int CR_THREAD::refresh_myrank_active(gaspi_rank_t new_myrank_active){
	myrank_active = new_myrank_active;
	if(crlib_verbose>=2) gaspi_printf("crlib: myrank: %d,\t myrank_active: %d\n", myrank, myrank_active);
	return 0;
}

void CR_THREAD::refresh_neighbours_list(gaspi_rank_t * failed_proc_list, gaspi_rank_t * rescue_proc_list){
	for(int i=1; i<=(int)failed_proc_list[0]; ++i){
		nodenames_working[(int)failed_proc_list[i]] = nodenames_all[rescue_proc_list[i]];
	}
	gaspi_printf("numprocs_working %d \n", numprocs_working);
	for(int i=0;i<numprocs_working;++i)
	{
		if(crlib_verbose>=1) gaspi_printf("crlib: nodenames_working[%d] %s\n", i, nodenames_working[i]);
	}
	for(int i=0;i<numprocs_working-1;++i){
		partner_nodenames[i]= nodenames_working[i+1];
	}
	partner_nodenames[numprocs_working-1] = nodenames_working[0];

	// create source_partner_nodenames_alive
	for(int i=1;i<numprocs_working;++i){
		source_partner_nodenames[i] = nodenames_working[i-1];
	}
	source_partner_nodenames[0]=nodenames_working[numprocs_working-1];
	
	if(status_processes[myrank]==WORKING_){
		gaspi_printf("crlib: After refresh_neighbours_list Myrank: %d, nodename: %s, partner_nodename: %s source_partner_nodenames: %s\n", myrank, mynodename(), partner_nodenames[myrank_active], source_partner_nodenames[myrank_active]);
	}
	if(myrank_active==0){
		write_partner_nodenames_info();
	}
	return; 
}

int CR_THREAD::write_partner_nodenames_info(){
	gaspi_printf("crlib: writing file now\n");
	char * crinfo_file_path = new char[128];
	sprintf(crinfo_file_path, "%s", perm_ckpt_dir_path);
	char * file_name 		= new char[128];
	sprintf(file_name, "%s/%s", crinfo_file_path, "crinfo.info");
	gaspi_printf("crlib: crinfo_file_name_is: %s\n", file_name);
	FILE * fp;
	if( NULL == (fp = fopen(file_name, "w")) ) {
		fprintf(stderr, "crlib: Error: Unable to open file (%s)\n", file_name);
		return -1;
	}
	for(int i=0; i<numprocs_working; ++i){
		fprintf(fp, "%s\n", nodenames_working[i]);
		gaspi_printf("crlib: nodenames_working[i]: %s\n", nodenames_working[i]);
	}
	
	gaspi_printf("crlib: writing file is done\n");
	fclose(fp);
	sync();
	return 0;
}

int CR_THREAD::read_partner_nodenames_info(){
	char * crinfo_file_path = new char[128];
	sprintf(crinfo_file_path, "%s", perm_ckpt_dir_path);
	char * file_name 		= new char[128];
	sprintf(file_name, "%s/%s", crinfo_file_path, "crinfo.info");

	FILE * fp;
	if( NULL == (fp = fopen(file_name, "r")) ) {
		fprintf(stderr, "Error: Unable to open file (%s)\n", file_name);
		return -1;
	}
	for(int i=0; i<numprocs_working; ++i){
		fscanf(fp, "%s\n", nodenames_working[i]);
		gaspi_printf("crlib: read nodenames_working is: %s\n", nodenames_working[i]);
	}
	fclose(fp);	
	make_partner_source_nodenames_from_nodenames_working();
	
	return 0;
}


int CR_THREAD::make_partner_source_nodenames_from_nodenames_working(){						
	for(int i=0;i<numprocs_working;++i)
	{
		if(crlib_verbose>=1) gaspi_printf("crlib: nodenames_worfking[%d] %s\n", i, nodenames_working[i]);
	}
	for(int i=0;i<numprocs_working-1;++i){
		partner_nodenames[i]= nodenames_working[i+1];
	}
	partner_nodenames[numprocs_working-1] = nodenames_working[0];

	// create source_partner_nodenames_alive
	for(int i=1;i<numprocs_working;++i){
		source_partner_nodenames[i] = nodenames_working[i-1];
	}
	source_partner_nodenames[0]=nodenames_working[numprocs_working-1];
	
	if(status_processes[myrank]==WORKING_){
		gaspi_printf("crlib: make_partner_source_nodenames_from_nodenames_working Myrank: %d, nodename: %s, partner_nodename: %s source_partner_nodenames: %s\n", myrank, mynodename(), partner_nodenames[myrank_active], source_partner_nodenames[myrank_active]);
	}
	return 0;
}


bool CR_THREAD::am_i_rescue_process_(gaspi_rank_t * rescue_proc_list){
	gaspi_printf("myrank: %d, numprocs: %d, numprocs_working %d \n ", myrank, numprocs, numprocs_working);
	bool am_i_rescue_proc_=false;
	for(gaspi_rank_t i=1; i<=rescue_proc_list[0]; ++i){
		if(myrank == rescue_proc_list[i]){
			am_i_rescue_proc_ = true;
		}
	}
	return am_i_rescue_proc_;
}

