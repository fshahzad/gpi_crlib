#ifndef __PINNING_H__
#define __PINNING_H__

int pin_current_thread(int rank, int local_rank, int thread_id, int * cpu);
int pin_current_master_thread(int rank, int local_rank, int thread_id, int * cpu);
int pin_current_thread_to_cpu(int * cpu);

#endif // __PINNING_H__
