#ifndef __CRLIB_H_INCLUDED__
#define __CRLIB_H_INCLUDED__

#include <GASPI.h>
#include <iomanip>
#include <omp.h>
#include "test_utils.h"
#include <sched.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include "pinning.h"
#include "timing.h"
#include <unistd.h>

#define CRLIB_GASPI_TIMEOUT_TIME 40000
static int crlib_verbose=5;


using namespace std;

void run_daemon_thread(void * arg);

class param;
class cp_param;

class CR_THREAD{
	public:
		CR_THREAD(gaspi_rank_t numprocs_working_);
		~CR_THREAD();
		
		gaspi_rank_t myrank_active;
		const gaspi_rank_t myrank, numprocs, numprocs_working;
		char ** nodenames_working, ** nodenames_all; // , ** nodenames_alive;//, **partner_nodenames_alive, **source_partner_nodenames_alive;
		char ** partner_nodenames, **source_partner_nodenames;
// 		char * source_partner_nodename;
// 		char * partner_nodename;
		char * nodename;
		
		char *machine_file_daemon;
		int restart;
		int to_neighbour, to_pfs;
		int program_running_flag;
		pthread_t * cr_pthread;
		char * perm_ckpt_dir_path;
		char * temp_ckpt_dir_path;
		
		char * main_ckpt_dir;
		char * username;
		int init(char * machine_file_daemon_, int CP_count_, int * status_processes, char * perm_ckpt_dir_path_, char * temp_ckpt_dir_path_);

		int make_nodelist();
		int make_partner_nodelist();

		char tomove_ckpt_dir_name[256];
		char prtr_ckpt_dir_path[256];
		

		int transfer_CP();
		volatile int CP_count_cr_thread;
		volatile int * status_processes;

		int transfer_CP_to_PFS();
		int trasfer_CP_to_neighbour();
		volatile int transfer_CP_flag;
		char* mynodename();
		
// 		int fetch_remote_cp(int new_myrank_active, gaspi_rank_t * rescue_proc_list, int cp_to_restart);
		int fetch_remote_cp(gaspi_rank_t new_myrank_active, gaspi_rank_t * rescue_proc_list);
		bool am_i_rescue_process_ (gaspi_rank_t * rescue_proc_list);

		void refresh_neighbours_list(gaspi_rank_t * failed_proc_list, gaspi_rank_t * rescue_proc_list);
		int refresh_nodelist(int failed_proc_num );
		

// 		int create_crinfo_file();		
// 		int refresh_create_crinfo_file();
// 		void run_daemon_thread(void * arg);
		int remove_prev_CP_neighbour();
		int is_CP_transfer_complete();
		int wait_for_CP_transfer();

		int write_partner_nodenames_info();
		int read_partner_nodenames_info();
		int make_partner_source_nodenames_from_nodenames_working();
		int refresh_myrank_active(gaspi_rank_t new_myrank_active);
// 		int remove_prev_CP_PFS();
// 		int print_health_vec();
// 		int send_msg_to_check_state();
// 		int check_comm_health();
// 		int update_status_processes_array();
// 		int check_and_bring_required_CP();
// 		int monitor_and_transfer_CP();
		
		
};

enum STATUS_PROCESSES{
	IDLE_ = 2,
	BROKEN_ = 1,
	WORKING_ = 0,
	WORKFINISHED_ = 9,
};

#endif